<?php
$table;
try {
	$db = new PDO("mysql:host=localhost;dbname=yabanji", "yabanji", "neto1643", array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
} catch (PDOException $e){
	die("Error: ".$e->getMessage());
}
if(!empty($_POST) && !empty($_POST['input'])){
	$table = special($_POST['input']);
    $db->query("CREATE TABLE ".$table.
		"(id int NOT NULL AUTO_INCREMENT,
		name varchar(100) NOT NULL,
		price float NOT NULL,
		PRIMARY KEY(id)
		 ) ENGINE=InnoDB DEFAULT CHARSET=utf8;");
}
function special($str){
    $str = htmlspecialchars($str);
    return $str;
}
?>

<!DOCTYPE html>
<html>
<head>
	<title>Table</title>
	<style type="text/css">
	   div {
	   	padding: 7px;
	    padding-right: 20px;
	    border: solid 1px black;
	    font-family: Verdana, Arial, Helvetica, sans-serif;
	    font-size: 13pt;
	   	background: #E6E6FA;
	  }
	  table {
        border-spacing: 0;
        border-collapse: collapse;
      }

      table td, table th {
        border: 1px solid #ccc;
        padding: 5px;
      }
      table th {
        background: #eee;
      }
	</style>
</head>
<body>
	<div align="center">
		<form enctype="multipart/form-data" method="POST" action="#" name="myform">
			<label for="input">Введите имя таблицы.</label>
			<input type="text" name="input">
			<input type="submit" name="button" value="Создать таблицу">
		</form>
	</div>
	<?php
	if(!empty($_GET)){
        if (!empty($_POST) && !empty($_POST['delete'])){
            $check = "ALTER TABLE ".$_GET['name']." DROP COLUMN ".$_POST['hiddenName'];
            $db->query($check);
        }
        if(!empty($_POST) && !empty($_POST['types'])){
            $check = "ALTER TABLE ".$_GET['name']." MODIFY ".$_POST['hiddenName']." "
            .$_POST['types']." NOT NULL";
            $db->query($check);
        }
        if(!empty($_POST) && !empty($_POST['inputName'])){
            $ch = special($_POST['inputName']);
            $check = "ALTER TABLE ".$_GET['name']." CHANGE ".$_POST['hiddenName']." "
            .$ch." ".$_POST['hiddenType'].";";
            $db->query($check);
        }
        try{
            $results = $db->query('DESCRIBE '.$_GET['name']);
        } catch (PDOException $e){
            die("Error: ".$e->getMessage());
        }
    ?>
    <table>
        <tr>
            <th>Наименование</th>
            <th>Тип</th>
            <th>Изменить название</th>
            <th>Изменить тип</th>
            <th>Удалить поле</th>
        </tr>
        <?php
        while ($row = $results->fetch(PDO::FETCH_ASSOC)){
        ?>
        <tr>
            <td><?= $row['Field'] ?></td>
            <td><?= $row['Type'] ?></td>
            <td>
                <form enctype="multipart/form-data" method="POST" action="#" name="changeName">
                    <input type="text" name="inputName" placeholder="Введите имя поля">
                    <input type="hidden" name="hiddenName" value="<?= $row['Field'] ?>">
                    <input type="hidden" name="hiddenType" value="<?= $row['Type'] ?>">
                    <input type="submit" name="chButtonName" value="Изменить">
                </form>
            </td>
            <td>
                <form enctype="multipart/form-data" method="POST" action="#" name="changeType">
                    <select name="types">
                        <option value="int">INT</option>
                        <option value="varchar(100)">VARCHAR</option>
                        <option value="float">FLOAT</option>
                    </select>
                    <input type="hidden" name="hiddenName" value="<?= $row['Field'] ?>">
                    <input type="submit" name="chButtonType" value="Изменить">
                </form>
            </td>
            <td>
                <center>
                    <form enctype="multipart/form-data" method="POST" action="#" name="delete">
                        <input type="hidden" name="hiddenName" value="<?= $row['Field'] ?>">
                        <input type="submit" name="delete" value="Удалить">
                    </form>
                </center>
            </td>
        </tr>
        <?php
        }
        ?>
    </table>
    <h3><a href="index.php">Назад</a></h3>
    <?php
		die();
	}
	try{
		$results = $db->query('SHOW TABLES');
	} catch (PDOException $e){
        die("Error: ".$e->getMessage());
    }
	?>
	<table>
		<tr>
			<th>Список таблиц в БД</th>
		</tr>
		<?php
    	while ($row = $results->fetch(PDO::FETCH_ASSOC)){
        ?>
		<tr>
			<td><a href='?name=<?= $row['Tables_in_yabanji'] ?>'>
				<?= $row['Tables_in_yabanji'] ?></a></td>
		</tr>
		<?php
    	}
		?>
	</table>
</body>
</html>